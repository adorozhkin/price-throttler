# CURRENCY RATE THROTTLER CODING TASK#

## DESCRIPTION ##
  PriceThrottler class implements the following requirements:
 - Distribute updates to its listeners which are added through subscribe() and removed through unsubscribe()
 - Some subscribers are very fast (i.e. onPrice() for them will only take a microsecond) and some are very slow (onPrice() might take 30 minutes). Imagine that some subscribers might be showing a price on a screen and some might be printing them on a paper
 - Some ccyPairs change rates 100 times a second and some only once or two times a day
 - ONLY LAST PRICE for each ccyPair matters for subscribers. I.e. if a slow processor is not coping with updates for EURUSD - it is only important to deliver the latest rate
 - It is important not to miss rarely changing prices. I.e. it is important to deliver EURRUB if it ticks once per day but you may skip some EURUSD ticking every second
 - You don't know in advance which ccyPair are frequent and which are rare. Some might become more frequent at different time of a day
 - You don't know in advance which of the subscribers are slow and which are fast.
 - Slow subscribers should not impact fast subscribers

 In short words the purpose of PriceThrottler is to solve for slow consumers
## BUILD ##
To build run following command
```
mvn clean package
```


## Implementation ##

Main class is org.adorozhkin.pricethrottler.PriceProcessorImpl it register all subscribers in org.adorozhkin.pricethrottler.Subscriber class.
org.adorozhkin.pricethrottler.Subscriber submit onPrice tasks into central executor (org.adorozhkin.pricethrottler.executor.NotificationExecutor) only if incoming ccyPair is not in Set of pending (not yet processed) updates

All subscribed priceProcessors receive updates from central org.adorozhkin.pricethrottler.executor.NotificationExecutor. It was decided not to use separated executor per PriceProcessor
to controll number of active threads. It there are too many very slow consumers than pool size should be increased to improve throughout put of fast consumers.