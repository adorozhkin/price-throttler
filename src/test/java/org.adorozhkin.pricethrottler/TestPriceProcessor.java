package org.adorozhkin.pricethrottler;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class TestPriceProcessor {

    enum CcyPairs {
        EURUSD(0, 10_000),
        RURUSD(100, 100),
        RUREUR(1000, 2);

        private final int delay;
        private final int maxValue;

        CcyPairs(int delay, int maxValue) {
            this.delay = delay;
            this.maxValue = maxValue;
        }
    }

    @Test
    public void testPriceProcessor() throws InterruptedException {
        PriceProcessor processor = new PriceProcessorImpl(1000, 3);

        Map<String, Map<String, List<Double>>> result = new ConcurrentHashMap<>();
        String consumerName = "consumer";

        Semaphore releaseConsumer = new Semaphore(0);
        Semaphore rateConsumed = new Semaphore(0);

        PriceProcessor consumer = new SlowConsumer(consumerName, releaseConsumer, rateConsumed, result);
        processor.subscribe(consumer);
        processor.onPrice(CcyPairs.EURUSD.name(), 1.0);
        releaseConsumer.release();
        rateConsumed.acquire();
        processor.onPrice(CcyPairs.EURUSD.name(), 2.0);
        releaseConsumer.release();
        rateConsumed.acquire();
        processor.onPrice(CcyPairs.EURUSD.name(), 3.0);
        releaseConsumer.release();
        rateConsumed.acquire();

        assertTrue("Consumer received ccyPair", result.containsKey(consumerName));
        Map<String, List<Double>> consumerResult = result.get(consumerName);
        assertTrue("Consumer received EURUSD", consumerResult.containsKey(CcyPairs.EURUSD.name()));
        assertEquals("All rates are received", 3, consumerResult.get(CcyPairs.EURUSD.name()).size());
    }

    @Test
    public void stressTest() throws InterruptedException, ExecutionException {
        PriceProcessor processor = new PriceProcessorImpl(1000, 3);

        Map<String, Map<String, List<Double>>> result = new ConcurrentHashMap<>();
        String slowConsumerName = "slow_consumer";
        String fastConsumerName = "fast_consumer";

        Semaphore releaseConsumer = new Semaphore(0);
        Semaphore rateConsumed = new Semaphore(0);

        PriceProcessor slowConsumer = new SlowConsumer(slowConsumerName, releaseConsumer, rateConsumed, result);
        PriceProcessor fastConsumer = new FastConsumer(fastConsumerName, result);
        processor.subscribe(slowConsumer);
        processor.subscribe(fastConsumer);

        CompletableFuture[] tasks = Arrays
                .stream(CcyPairs.values())
                .map(ccyPair -> CompletableFuture.runAsync(() -> IntStream.rangeClosed(0, ccyPair.maxValue).forEach(i -> sendRateWithDelay(processor, ccyPair.name(), i, ccyPair.delay))))
                .toArray(CompletableFuture[]::new);

        CompletableFuture.allOf(tasks).get();

        releaseConsumer.release();
        rateConsumed.acquire();

        assertTrue(slowConsumerName + " received ccyPair", result.containsKey(slowConsumerName));
        assertTrue(fastConsumerName + " received ccyPair", result.containsKey(fastConsumerName));
        Map<String, List<Double>> slowConsumerResult = result.get(slowConsumerName);
        for (int i = 0; i < 100 && slowConsumerResult.size() < CcyPairs.values().length; i++) {
            Thread.sleep(100);
            releaseConsumer.release();
            rateConsumed.acquire();
            slowConsumerResult = result.get(slowConsumerName);
        }


        Map<String, List<Double>> fastConsumerResult = result.get(fastConsumerName);

        assertEquals(slowConsumerName + " received all rates", CcyPairs.values().length, slowConsumerResult.size());
        assertEquals(fastConsumerName + " received all rates", CcyPairs.values().length, fastConsumerResult.size());

        for (CcyPairs ccyPair : CcyPairs.values()) {
            assertTrue(slowConsumerName + " contains " + ccyPair + " " + ccyPair.maxValue, slowConsumerResult.get(ccyPair.name()).contains((double) ccyPair.maxValue));
            assertTrue(fastConsumerName + " contains " + ccyPair + " " + ccyPair.maxValue, fastConsumerResult.get(ccyPair.name()).contains((double) ccyPair.maxValue));
        }
    }

    @Test
    public void testUnsubscribeConsumers() throws InterruptedException {
        PriceProcessor processor = new PriceProcessorImpl(1000, 3);

        Map<String, Map<String, List<Double>>> result = new ConcurrentHashMap<>();
        String consumer1Name = "consumer1";
        String consumer2Name = "consumer2";

        Semaphore releaseConsumer1 = new Semaphore(0);
        Semaphore rateConsumed1 = new Semaphore(0);
        Semaphore releaseConsumer2 = new Semaphore(0);
        Semaphore rateConsumed2 = new Semaphore(0);

        PriceProcessor consumer1 = new SlowConsumer(consumer1Name, releaseConsumer1, rateConsumed1, result);
        PriceProcessor consumer2 = new SlowConsumer(consumer2Name, releaseConsumer2, rateConsumed2, result);
        processor.subscribe(consumer1);
        processor.subscribe(consumer2);

        processor.onPrice(CcyPairs.EURUSD.name(), 1.0);
        releaseConsumer1.release();
        releaseConsumer2.release();
        rateConsumed1.acquire();
        rateConsumed2.acquire();

        processor.unsubscribe(consumer2);

        processor.onPrice(CcyPairs.RURUSD.name(), 2.0);
        releaseConsumer1.release();
        releaseConsumer2.release();
        rateConsumed1.acquire();

        assertTrue(consumer1Name + " received ccyPair", result.containsKey(consumer1Name));
        assertTrue(consumer2Name + " received ccyPair", result.containsKey(consumer2Name));
        Map<String, List<Double>> consumer1Result = result.get(consumer1Name);
        Map<String, List<Double>> consumer2Result = result.get(consumer2Name);
        assertTrue(consumer1Name + " received EURUSD", consumer1Result.containsKey(CcyPairs.EURUSD.name()));
        assertTrue(consumer2Name + " received EURUSD", consumer2Result.containsKey(CcyPairs.EURUSD.name()));
        assertTrue(consumer1Name + " received RURUSD", consumer1Result.containsKey(CcyPairs.RURUSD.name()));
        assertFalse(consumer2Name + " didn't received RURUSD", consumer2Result.containsKey(CcyPairs.RURUSD.name()));
    }

    private void sendRateWithDelay(PriceProcessor processor, String ccyPair, int rate, int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        processor.onPrice(ccyPair, rate);
    }

    private static class SlowConsumer implements PriceProcessor {
        private final String name;
        private final Semaphore releaseConsumer;
        private final Semaphore rateConsumed;
        private final Map<String, Map<String, List<Double>>> result;

        private SlowConsumer(String name, Semaphore releaseConsumer, Semaphore rateConsumed, Map<String, Map<String, List<Double>>> result) {
            this.name = name;
            this.releaseConsumer = releaseConsumer;
            this.rateConsumed = rateConsumed;
            this.result = result;
        }

        @Override
        public void onPrice(String ccyPair, double rate) {
            try {
                releaseConsumer.acquire();
                result.computeIfAbsent(name, key -> new ConcurrentHashMap<>()).computeIfAbsent(ccyPair, key -> new CopyOnWriteArrayList<>()).add(rate);
                rateConsumed.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void subscribe(PriceProcessor priceProcessor) {

        }

        @Override
        public void unsubscribe(PriceProcessor priceProcessor) {

        }
    }

    private static class FastConsumer implements PriceProcessor {
        private final String name;
        private final Map<String, Map<String, List<Double>>> result;

        private FastConsumer(String name, Map<String, Map<String, List<Double>>> result) {
            this.name = name;
            this.result = result;
        }

        @Override
        public void onPrice(String ccyPair, double rate) {
            result.computeIfAbsent(name, key -> new ConcurrentHashMap<>()).computeIfAbsent(ccyPair, key -> new CopyOnWriteArrayList<>()).add(rate);
        }

        @Override
        public void subscribe(PriceProcessor priceProcessor) {

        }

        @Override
        public void unsubscribe(PriceProcessor priceProcessor) {

        }
    }
}
