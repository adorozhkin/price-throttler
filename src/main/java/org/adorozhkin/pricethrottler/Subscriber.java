package org.adorozhkin.pricethrottler;

import org.adorozhkin.pricethrottler.executor.ExecutionTask;
import org.adorozhkin.pricethrottler.executor.NotificationExecutor;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;

/**
 * Holds information about registered price subscribers. Prepare onPrice tasks for executor
 */
public class Subscriber {
    private final PriceProcessor processor;
    private final NotificationExecutor executor;
    private final CcyStore ccyStore;
    private final ConcurrentMap<String, Future<?>> pendingTasks = new ConcurrentHashMap<>();

    public Subscriber(PriceProcessor processor, NotificationExecutor executor, CcyStore ccyStore) {
        this.processor = processor;
        this.executor = executor;
        this.ccyStore = ccyStore;
    }

    /**
     * Creates executor task only in case of such currency pair is not in pending tasks
     *
     * @param ccyPair
     */
    public void onPrice(String ccyPair) {
        pendingTasks.computeIfAbsent(ccyPair, key -> executor.submit(new ExecutionTask(this, ccyStore, ccyPair)));
    }

    public void execute(String ccyPair, double rate) {
        pendingTasks.remove(ccyPair);
        processor.onPrice(ccyPair, rate);
    }
}
