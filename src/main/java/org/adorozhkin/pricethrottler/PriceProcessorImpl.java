package org.adorozhkin.pricethrottler;

import org.adorozhkin.pricethrottler.executor.NotificationExecutor;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Price throttler implementation. Registers all subscribers and propagate currency pair updates to them
 */
public class PriceProcessorImpl implements PriceProcessor {

    private final NotificationExecutor executor;
    private final CcyStore ccyStore = new CcyStore();
    private final ConcurrentMap<PriceProcessor, Subscriber> subscribers = new ConcurrentHashMap<>();


    public PriceProcessorImpl(int subscriberQueueSize, int executorThreads) {
        this.executor = new NotificationExecutor(executorThreads, subscriberQueueSize);
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        ccyStore.updateRate(ccyPair, rate);
        subscribers.values().forEach(subscriber -> subscriber.onPrice(ccyPair));
    }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        subscribers.put(priceProcessor, new Subscriber(priceProcessor, executor, ccyStore));
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) {
        subscribers.remove(priceProcessor);
    }

}
