package org.adorozhkin.pricethrottler.executor;

import org.adorozhkin.pricethrottler.CcyStore;
import org.adorozhkin.pricethrottler.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecutionTask implements Runnable {
    private final static Logger log = LoggerFactory.getLogger(ExecutionTask.class);

    private final Subscriber processor;
    private final CcyStore ccyStore;
    private final String ccyPair;

    public ExecutionTask(Subscriber processor, CcyStore ccyStore, String ccyPair) {
        this.processor = processor;
        this.ccyStore = ccyStore;
        this.ccyPair = ccyPair;
    }

    @Override
    public void run() {
        try {
            processor.execute(ccyPair, ccyStore.getRate(ccyPair));
        } catch (Exception e) {
            log.error("Failed to execute task {} for {}", ccyPair, processor, e);
        }
    }

}
