package org.adorozhkin.pricethrottler.executor;

import java.util.concurrent.*;

/**
 * Central executor of all rate updates requests. Configured (number of threads and incoming queue size) externally
 */
public class NotificationExecutor {

    private final ExecutorService executor;
    private final BlockingQueue<Runnable> tasks;

    public NotificationExecutor(int threads, int queueSize) {
        tasks = new ArrayBlockingQueue<>(queueSize);
        executor = new ThreadPoolExecutor(threads, threads, 1, TimeUnit.MINUTES, tasks);
    }

    public Future<?> submit(ExecutionTask task) {
        return executor.submit(task);
    }
}
