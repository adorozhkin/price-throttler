package org.adorozhkin.pricethrottler;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Store latest currency pairs rates
 */
public class CcyStore {
    private final ConcurrentMap<String, Double> ccyStore = new ConcurrentHashMap<>();

    public double getRate(String ccyPair) {
        return ccyStore.get(ccyPair);
    }

    public void updateRate(String ccyPair, double rate) {
        ccyStore.put(ccyPair, rate);
    }

}
